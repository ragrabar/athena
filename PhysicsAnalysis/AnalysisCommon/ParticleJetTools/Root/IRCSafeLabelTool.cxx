/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/


// This will obviously have to be modified
#include "ParticleJetTools/IRCSafeLabelTool.h"
#include "ParticleJetTools/ParticleJetLabelCommon.h"
#include "xAODJet/Jet.h"
#include "xAODJet/JetContainer.h"
#include "xAODJet/JetAuxContainer.h"
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthParticleAuxContainer.h"
#include "AsgDataHandles/ReadHandle.h"
#include "AsgMessaging/Check.h"
#include "AsgTools/ToolHandle.h"
#include "radekjet/ClusterSequence.hh"
#include "radekjet/PseudoJet.hh"
#include "radekjet/JetDefinition.hh"
#include "FlavNeutraliserPlugin.hh"

 #include <iostream>
 #include <fstream>

using namespace std;
using namespace xAOD;
using namespace radekjet;
using namespace radekjet::contrib;

IRCSafeLabelTool::IRCSafeLabelTool(const std::string& name)
        : AsgTool(name) {
    declareProperty("LabelNameIFN", m_ircsafelabelnames.IFNsingleint="", "Name of the jet label attribute to be added. (IFN)");
    declareProperty("DoubleLabelNameIFN", m_ircsafelabelnames.IFNdoubleint="", "Name of the jet label attribute to be added (with the possibility of up to 2 matched hadrons). (IFN)");
    declareProperty("LabelNameCMP", m_ircsafelabelnames.CMPsingleint="", "Name of the jet label attribute to be added. (CMP)");
    declareProperty("DoubleLabelNameCMP", m_ircsafelabelnames.CMPdoubleint="", "Name of the jet label attribute to be added (with the possibility of up to 2 matched hadrons). (CMP)");
    declareProperty("LabelNameGHS", m_ircsafelabelnames.GHSsingleint="", "Name of the jet label attribute to be added. (GHS)");
    declareProperty("DoubleLabelNameGHS", m_ircsafelabelnames.GHSdoubleint="", "Name of the jet label attribute to be added (with the possibility of up to 2 matched hadrons). (GHS)");
//    declareProperty("LabelPtName", m_labelnames.pt="HadronConeExclTruthLabelPt", "Name of attribute for labelling particle pt");
//    declareProperty("LabelPtScaledName", m_labelnames.pt_scaled="HadronConeExclTruthLabelPtScaled", "Name of attribute for labelling particle pt divided by jet pt");
//    declareProperty("LabelLxyName", m_labelnames.Lxy="HadronConeExclTruthLabelLxy", "Name of attribute for Lxy of labelling particle");
//    declareProperty("LabelDRName", m_labelnames.dr="HadronConeExclTruthLabelDR", "Name of attribute for dR(pseudojet, jet) for labelling particle");
//    declareProperty("LabelPdgIdName", m_labelnames.pdgId="HadronConeExclTruthLabelPdgId", "Name of attribute for pdgID of labelling particle");
//    declareProperty("OldLabelName", m_labelnames.old_label="HadronConeExclTruthLabelOldID", "Name of jet label as assigned by the old deltaR definition"); //RG: adding a new one to keep track of old definition
//    declareProperty("BLabelName", m_bottomlabelname="", "Name of the attribute to be added for matched B hadrons.");
//    declareProperty("CLabelName", m_charmlabelname="", "Name of the attribute to be added for matched C hadrons.");
//    declareProperty("TauLabelName", m_taulabelname="", "Name of the attribute to be added for matched taus.");
    declareProperty("TruthJetPtMin", m_truthjetptmin=5000, "Minimum pT of truth jets for labeling (MeV)");
    declareProperty("JetPtMin", m_jetptmin=10000, "Minimum pT of jets to be lebeled (MeV)");
    declareProperty("DRMax", m_drmax=0.3, "Maximum deltaR between a particle and jet to be labeled");
    declareProperty("MatchMode", m_matchmode="MinDR",
            "In the case that a particle matches two jets, the closest (MatchMode=MinDR) or highest-pT (MatchMode=MaxPt) jet will be labeled");
}

//FlavInfo FlavInfoFromTruth(const TruthParticle* truthpart) {
//    // The way we assign FlavInfo here is *only* justified if we use flavour_modulo_2 recombination in
//    // the jet flavour algorithms. This is only the first iteration of doing things for tests.
//    if (truthpart->isBottomHadron()) {
//        return FlavInfo(5);
//
//    } else if (truthpart->isCharmHadron()) {
//        return FlavInfo(4);
//
//    } else {
//        // Neglect other flavours (for now!)
//        return FlavInfo(0);
//
//    }
//
//}

vector<PseudoJet> GetJetInputs(const TruthParticleContainer& parts, const TruthParticleContainer& label_bs, const TruthParticleContainer& label_cs, double ptmin) {
  // This function iterates over all jet constituents and creates
  // pseudojets with identical p_t. It is good to copy them to pseudojets
  // since all flavour algorithms can be sped up using the nnh class in
  // fastjet
    ofstream tellme2;
    tellme2.open("pseudojets.txt");
  // We only want the original parent B, without its children as jet inputs
  vector<const TruthParticle*> bs, cs, truthparts;
  for (TruthParticleContainer::const_iterator part_itr = parts.begin();
          part_itr != parts.end(); ++part_itr) {
            const TruthParticle* part = (*part_itr);
            truthparts.push_back(part);
  }
    for (TruthParticleContainer::const_iterator labels_itr = label_bs.begin();
          labels_itr != label_bs.end(); ++labels_itr) {
            const TruthParticle* part = (*labels_itr);
            bs.push_back(part);
  }
    for (TruthParticleContainer::const_iterator labels_itr = label_cs.begin();
          labels_itr != label_cs.end(); ++labels_itr) {
            const TruthParticle* part = (*labels_itr);
            cs.push_back(part);
  }
  tellme2 << "elo" << endl;
  using ParticleJetTools::childrenRemoved;
  childrenRemoved(bs, bs);                //||---|
  childrenRemoved(bs, cs);                //||---|
  childrenRemoved(bs, truthparts);        //||---|--- All this will require rethinking, as I am sure it can be optimized
  childrenRemoved(cs, cs);                //||---|
  childrenRemoved(cs, truthparts);        //||---|
  tellme2 << "no siema co tam" << endl;
  if (bs.size() > 0)
  truthparts.insert(truthparts.end(), bs.begin(), bs.end());
  if (cs.size() > 0)
  truthparts.insert(truthparts.end(), cs.begin(), cs.end());
  vector<PseudoJet> fullevent(truthparts.size());
  tellme2 << "a nic nudy" << endl;
  // My understanding is that we *never* see a B or a C hadron directly in the final state, so a
  // case where truthparts and bs contain the same particle does not have to be accounted for.
  for (unsigned int ip = 0; ip < truthparts.size(); ip++) {
    const TruthParticle* part = truthparts[ip];
    double px = part->px();
    double py = part->py();
    double pz = part->pz();
    double E = part->e();
    tellme2 << E << py << pz << px << endl;
    fullevent[ip] = PseudoJet(px,py,pz,E);
    //p.set_user_info(std::make_unique<FlavHistory>(FlavInfoFromTruth(truthparts[ip])).release());
    if (part->isCharmHadron()) fullevent[ip].set_user_info(new FlavInfo(4));      //||    For now, (bc) hadrons are NOT 
    if (part->isBottomHadron()) fullevent[ip].set_user_info(new FlavInfo(5));     //||    handled (they are treated as b)
    if (!part->isBottomHadron() && !part->isCharmHadron()) fullevent[ip].set_user_info(new FlavInfo(0));
    }
  
        //Next: do fastjet stuff!-
      //auto flav_recombiner = new FlavRecombiner();
      JetDefinition jet_def = JetDefinition(antikt_algorithm, 0.4);
      //jet_def.set_recombiner(flav_recombiner);
      // FlavNeutraliser arguments: jet definition; p; q; a; modulo 2 flavour = true.
      // Current FlavNeutraliser arguments: alpha, omega, flav_summation (use_mass_flav = false)
      auto FNPlugin = new  FlavNeutraliserPlugin(jet_def, 1.0, 2.0, FlavRecombiner::modulo_2);
      //auto FNPlugin = new FlavNeutraliserPlugin(jet_def);
      //FNPlugin->set_recursive(true);
      //FNPlugin->set_measure(FlavNeutraliser::maxscale);
      JetDefinition flav_neut_jet_def(FNPlugin);
      //JetDefinition jet_def_new = JetDefinition(new FlavNeutraliserPlugin(jet_def, 0.5, 0.5, 2, true));
      ClusterSequence neutralised_clustering(fullevent, flav_neut_jet_def);
      Selector select_pt = SelectorPtMin(ptmin);
      vector<PseudoJet> selected_pseudojets = select_pt(neutralised_clustering.inclusive_jets());
//      OTHER ALGORITHMS GO HERE   (SEE PANSCALES TESTS)
//      OTHER ALGORITHMS GO HER  (SEE PANSCALES TESTS)
//      OTHER ALGORITHMS GO HERE  (SEE PANSCALES TESTS)
//      OTHER ALGORITHMS GO HERE  (SEE PANSCALES TESTS)
      //      OTHER ALGORITHMS GO HERE (SEE PANSCALES TESTS)
      flav_neut_jet_def.delete_plugin_when_unused();
   //   vector<PseudoJet> tagged_pseudojets;
   //   for (unsigned int i_jets = 0; i_jets < selected_pseudojets.size(); i_jets++) {
   //     PseudoJet p = selected_pseudojets[i_jets];
   //     if(!p.user_info<FlavHistory>().current_flavour().is_flavourless()) {
   //       tagged_pseudojets.push_back(p);
   //     }
   //   }
    //  cout << tagged_pseudojets.size() << endl;
      // Create a container for tagged "truth" (with undecayed B hadrons) jets:
   //   JetContainer* bjets = new JetContainer;
      // JetAuxContainer bjetsaux;
      // Plan for Monday: Stop doing this. Simply return pseudojets and use those in the 
      // match function.
    //  bjets->setStore( new JetAuxContainer ); //setStore( &bjetsaux )
    //  PseudoJet p;
    //  for (unsigned int i_jets = 0; i_jets < tagged_pseudojets.size(); i_jets++) {
    //      p = tagged_pseudojets[i_jets];
    //      tellme2 << p.E() <<" " <<  p.py() << " " << p.pz() << " " << p.px() <<  " " << FlavHistory::current_flavour_of(p).description() << endl;
    //      if(!p.user_info<FlavHistory>().current_flavour().is_flavourless()) {
    //          Jet* tagged_jet = new Jet;
    //          tagged_jet->setJetP4(JetFourMom_t(p.pt(), p.eta(), p.phi(), p.m())); //pt, eta, phi, m
    //          bjets->push_back(tagged_jet);  
    //      }
    //  }
  // tellme2 << "cojest kurwa" << endl;
  //tellme2 << tagged_pseudojets.size();
      tellme2.close();
  return selected_pseudojets;
}

 void setJetIRCSafeLabels(const xAOD::Jet& jet,
                    const ParticleJetTools::Tag_PseudoJets& tag_pjets,
                    const ParticleJetTools::IRCSafeLabelDecorators& decs) {
        // if two pjets are matched, we will choose the one with larger pt
    auto getMaxPtPjet = [](const auto& container) -> const radekjet::PseudoJet* {
      if (container.size() == 0) return nullptr;
      auto itr = std::max_element(container.begin(), container.end(),
                                  [](auto* p1, auto* p2) {
                                    return p1->pt() < p2->pt();
                                  });
      return *itr;
    };

    // set new truth label for jets above pt threshold
    // hierarchy: b > c > tau > light
    // to do: write a lambda function for this
    int IFN_label = 0; // default: light
//    const radekjet::PseudoJet* IFN_labelling_pjet = nullptr;
//    const radekjet::PseudoJet* CMP_labelling_pjet = nullptr;
//    const radekjet::PseudoJet* GHS_labelling_pjet = nullptr;
    if (tag_pjets.IFN_b.size()) {
      IFN_label = 5;
//      IFN_labelling_pjet = getMaxPtPjet(tag_pjets.IFN_b);
    } else if (tag_pjets.IFN_c.size()) { 
      IFN_label = 4;
//      IFN_labelling_pjet = getMaxPtPjet(tag_pjets.IFN_c);
    } 
    int CMP_label = 0; // default: light
    if (tag_pjets.CMP_b.size()) {
      CMP_label = 5;
 //     CMP_labelling_pjet = getMaxPtPjet(tag_pjets.CMP_b);
    } else if (tag_pjets.CMP_c.size()) { 
      CMP_label = 4;
 //     CMP_labelling_pjet = getMaxPtPjet(tag_pjets.CMP_c);
    } 
    int GHS_label = 0; // default: light
    if (tag_pjets.GHS_b.size()) {
      GHS_label = 5;
//      GHS_labelling_pjet = getMaxPtPjet(tag_pjets.GHS_b);
    } else if (tag_pjets.GHS_c.size()) { 
      GHS_label = 4;
//      GHS_labelling_pjet = getMaxPtPjet(tag_pjets.GHS_c);
    } 

    //else if (tag_pjets.tau.size()) {
      //label = 15;
      //labelling_pjet = getMaxPtPart(particles.tau);
    //}

    // set old truth label for jets above pt threshold
    //int old_label = 0;
    //const xAOD::TruthParticle* labelling_particle = nullptr; //not used for now
    //if (tag_pjets.b.size()) {
    //  old_label = 5;
    //  labelling_particle = getMaxPtPart(tag_pjets.b);
    //} else if (tag_pjets.c.size()) {
    //  old_label = 4;
    //  labelling_particle = getMaxPtPart(tag_pjets.c);
    //} else if (tag_pjets.tau.size()) {
    //  old_label = 15;   // I decided to use the old truth label
    //  label = 15;       // procedure for taus
    //  labelling_particle = getMaxPtPart(tag_pjets.tau);
    //}

    // decorate info about the label

    decs.IFNsingleint(jet) = IFN_label;
    decs.CMPsingleint(jet) = CMP_label;
    decs.GHSsingleint(jet) = GHS_label;
    // RG: trying to add a new decoration about the old label (didnt work)
    //decs.old_label(jet) = old_label;
 //   if (label == 0) {
 //     decs.pt(jet) = -1;        // RG: trying to see if setting this to -1
 //     decs.pt_scaled(jet) = -1; // helps with ROOT plotting
 //     decs.Lxy(jet) = NAN;
 //     decs.dr(jet) = NAN;
 //     decs.pdgId(jet) = old_label;
 //   } else {
 //     decs.pt(jet) = pjetPt(labelling_pjet);
 //     decs.pt_scaled(jet) = pjetPt(labelling_pjet) / jet.pt();
 //     decs.Lxy(jet) = NAN; //partLxy(labelling_particle);     ---|
 //     decs.dr(jet) = NAN; // partDR(labelling_particle, jet); ---|---- To be implemented 
 //     decs.pdgId(jet) = old_label;// partPdgId(labelling_particle);   ---|
 //   }

    // extended flavour label
       // extended flavour label
    if (tag_pjets.IFN_b.size()) {
      if (tag_pjets.IFN_b.size() >= 2)
        decs.IFNdoubleint(jet) = 55;

      else if (tag_pjets.IFN_c.size())
        decs.IFNdoubleint(jet) = 54;

      else
        decs.IFNdoubleint(jet) = 5;

    } else if (tag_pjets.IFN_c.size()) {
      if (tag_pjets.IFN_c.size() >= 2)
        decs.IFNdoubleint(jet) = 44;

      else
        decs.IFNdoubleint(jet) = 4;

    }

    else
      decs.IFNdoubleint(jet) = 0;

    if (tag_pjets.CMP_b.size()) {
      if (tag_pjets.CMP_b.size() >= 2)
        decs.CMPdoubleint(jet) = 55;

      else if (tag_pjets.CMP_c.size())
        decs.CMPdoubleint(jet) = 54;

      else
        decs.CMPdoubleint(jet) = 5;

    } else if (tag_pjets.CMP_c.size()) {
      if (tag_pjets.CMP_c.size() >= 2)
        decs.CMPdoubleint(jet) = 44;

      else
        decs.CMPdoubleint(jet) = 4;

    }

    else
      decs.CMPdoubleint(jet) = 0;

    if (tag_pjets.GHS_b.size()) {
      if (tag_pjets.GHS_b.size() >= 2)
        decs.GHSdoubleint(jet) = 55;

      else if (tag_pjets.GHS_c.size())
        decs.GHSdoubleint(jet) = 54;

      else
        decs.GHSdoubleint(jet) = 5;

    } else if (tag_pjets.GHS_c.size()) {
      if (tag_pjets.GHS_c.size() >= 2)
        decs.GHSdoubleint(jet) = 44;

      else
        decs.GHSdoubleint(jet) = 4;

    }

    else
      decs.GHSdoubleint(jet) = 0;
  }

namespace{

}
StatusCode IRCSafeLabelTool::initialize(){
ATH_MSG_DEBUG(" Initializing... ");
  // initialize truth jet inputs key
  ATH_CHECK(m_outTruthPartKey.initialize());
  ATH_CHECK(m_tauPartCollectionKey.initialize());
  ATH_CHECK(m_bottomPartCollectionKey.initialize());
  ATH_CHECK(m_charmPartCollectionKey.initialize());

  // get the keys to the containers we just read, to build element
  // links later
  //
  //using Linker = ParticleJetTools::IParticleLinker;
  //m_blinker.reset(new Linker(m_bottomPartCollectionKey, m_bottomlabelname));
  //m_clinker.reset(new Linker(m_charmPartCollectionKey, m_charmlabelname));
  //m_taulinker.reset(new Linker(m_tauPartCollectionKey, m_taulabelname));

  // build label decorators
  m_ircsafelabeldecs.reset(new ParticleJetTools::IRCSafeLabelDecorators(m_ircsafelabelnames));
  return StatusCode::SUCCESS;
}


StatusCode IRCSafeLabelTool::decorate(const JetContainer& jets) const {

  ATH_MSG_VERBOSE("In " << name() << "::modify()");
    ofstream tellme;
    tellme.open("decorated_cout.txt");
    tellme << "Decorated activated! " << endl;
    // Retrieve the particle and jet containers
    ATH_MSG_DEBUG(" Decorating..." );
    SG::ReadHandle<xAOD::TruthParticleContainer> truthPartReadHandle(m_outTruthPartKey);
    SG::ReadHandle<xAOD::TruthParticleContainer> truthtausReadHandle(m_tauPartCollectionKey);
    SG::ReadHandle<xAOD::TruthParticleContainer> truthbsReadHandle(m_bottomPartCollectionKey);
    SG::ReadHandle<xAOD::TruthParticleContainer> truthcsReadHandle(m_charmPartCollectionKey);

    if (!truthtausReadHandle.isValid()){
      ATH_MSG_DEBUG(" Invalid ReadHandle for xAOD::ParticleContainer with key: " << truthtausReadHandle.key());
      return StatusCode::FAILURE;
    }

    if (!truthbsReadHandle.isValid()){
      ATH_MSG_DEBUG(" Invalid ReadHandle for xAOD::ParticleContainer with key: " << truthbsReadHandle.key());
      return StatusCode::FAILURE;
    }

    if (!truthcsReadHandle.isValid()){
      ATH_MSG_DEBUG(" Invalid ReadHandle for xAOD::ParticleContainer with key: " << truthcsReadHandle.key());
      return StatusCode::FAILURE;
    }
    if (!truthPartReadHandle.isValid()){
      ATH_MSG_DEBUG(" Invalid ReadHandle for xAOD::ParticleContainer with key: " << truthPartReadHandle.key());
      tellme << "wrong truth parts" << endl;
      return StatusCode::FAILURE;
    }
    tellme << "right truth parts" << endl;
    tellme << "przed scierwem pierdolonym" << endl;
    vector<PseudoJet> selected_pseudojets = GetJetInputs(*truthPartReadHandle, *truthbsReadHandle, *truthcsReadHandle, m_truthjetptmin/1000);
    vector<PseudoJet> btagged_pseudojets, ctagged_pseudojets;
      for (unsigned int i_jets = 0; i_jets < selected_pseudojets.size(); i_jets++) {
        PseudoJet p = selected_pseudojets[i_jets]; 
        if(p.user_info<FlavHistory>().current_flavour()[5] > 0) btagged_pseudojets.push_back(p); // things with [bc] flavour are pushed to *both*, and then
        if(p.user_info<FlavHistory>().current_flavour()[4] > 0) ctagged_pseudojets.push_back(p); // the b > c > tau hierarchy decides the final flavour
      }
    // match the tagged pseudojets to reco jets
    vector< vector<const PseudoJet*> > jetlabelIFN_b = match(btagged_pseudojets, jets);
    vector< vector<const PseudoJet*> > jetlabelIFN_c = match(ctagged_pseudojets, jets);
    vector< vector<const PseudoJet*> > jetlabelCMP_b(jets.size(), vector<const PseudoJet*>());// THEY ARE EMPTY FOR NOW ! ! ! ! ! 
    vector< vector<const PseudoJet*> > jetlabelCMP_c(jets.size(), vector<const PseudoJet*>());// THEY ARE EMPTY FOR NOW ! ! ! ! ! 
    vector< vector<const PseudoJet*> > jetlabelGHS_b(jets.size(), vector<const PseudoJet*>());// THEY ARE EMPTY FOR NOW ! ! ! ! ! 
    vector< vector<const PseudoJet*> > jetlabelGHS_c(jets.size(), vector<const PseudoJet*>());// THEY ARE EMPTY FOR NOW ! ! ! ! ! 
//
//
    //
    //vector< vector<const PseudoJet*> > jetlabeltau = match(tagged_jets, jets);

    // For old labels:
    //vector<vector<const TruthParticle*> > jetlabelpartsb = match(*truthbsReadHandle, jets);
    //vector<vector<const TruthParticle*> > jetlabelpartsc = match(*truthcsReadHandle, jets);
    //vector<vector<const TruthParticle*> > jetlabelpartstau = match(*truthtausReadHandle, jets);



    for (unsigned int iJet = 0; iJet < jets.size(); iJet++) {
    //    // remove children whose parent hadrons are also in the jet.
    //    // don't care about double tau jets
    //    // so leave them for now.

      //using ParticleJetTools::childrenRemoved;
      //childrenRemoved(jetlabelpartsb[iJet], jetlabelpartsb[iJet]);
      //childrenRemoved(jetlabelpartsb[iJet], jetlabelpartsc[iJet]);
      //childrenRemoved(jetlabelpartsc[iJet], jetlabelpartsc[iJet]);
//
       const Jet& jet = *jets.at(iJet);
//
      //  m_blinker->decorate(jet, jetlabelpartsb.at(iJet));
      // m_clinker->decorate(jet, jetlabelpartsc.at(iJet));
      // m_taulinker->decorate(jet, jetlabelpartstau.at(iJet));
//
    //    // set truth label to -99 for jets below pt threshold
        if (jet.pt() < m_jetptmin) {
            m_ircsafelabeldecs->IFNsingleint(jet) = -99;
            m_ircsafelabeldecs->IFNdoubleint(jet) = -99;

            m_ircsafelabeldecs->CMPsingleint(jet) = -99;
            m_ircsafelabeldecs->CMPdoubleint(jet) = -99;

            m_ircsafelabeldecs->GHSsingleint(jet) = -99;
            m_ircsafelabeldecs->GHSdoubleint(jet) = -99;
            continue;
        }

        // set truth label for jets above pt threshold
        // hierarchy: b > c > tau > light
        ParticleJetTools::Tag_PseudoJets tag_pjet;
        tag_pjet.IFN_b = jetlabelIFN_b.at(iJet);
        tag_pjet.IFN_c = jetlabelIFN_c.at(iJet);

        tag_pjet.CMP_b = jetlabelCMP_b.at(iJet);// THEY ARE EMPTY FOR NOW ! ! ! !
        tag_pjet.CMP_c = jetlabelCMP_c.at(iJet);// THEY ARE EMPTY FOR NOW ! ! ! !
        
        tag_pjet.GHS_b = jetlabelGHS_b.at(iJet);// THEY ARE EMPTY FOR NOW ! ! ! ! 
        tag_pjet.GHS_c = jetlabelGHS_c.at(iJet);// THEY ARE EMPTY FOR NOW ! ! ! ! 

//        ParticleJetTools::Particles particles; (RG: 26.06.2023 moving things around)
//        particles.b = jetlabelpartsb.at(iJet);
//        particles.c = jetlabelpartsc.at(iJet);
//        particles.tau = jetlabelpartstau.at(iJet);

        setJetIRCSafeLabels(jet, tag_pjet, *m_ircsafelabeldecs); //THIS WILL REQUIRE CHANGE (RG: I think done)
    }
   tellme.close();
    return StatusCode::SUCCESS;

}


vector< vector<const PseudoJet*> >
IRCSafeLabelTool::match(
        vector<PseudoJet>& tagged_pseudojets,
        const JetContainer& jets) const {
        ofstream tellme;
    tellme.open("decorated_cout.txt");
    tellme << "match activated! " << endl;
  ATH_MSG_VERBOSE("In " << name() << "::match()");
  
    vector< vector<const PseudoJet*> > jetlabelparts(jets.size(), vector<const PseudoJet*>());

    // determine the match mode
    bool mindrmatch;
    if (m_matchmode == "MinDR")
        mindrmatch = true;
    else if (m_matchmode == "MaxPt")
        mindrmatch = false;
    else {
        ATH_MSG_FATAL("MatchMode must be MinDR or MaxPt");
        return jetlabelparts;
    }

    // loop over pseudojets and find the best matched jet
    for (unsigned int i_tj = 0;
            i_tj < tagged_pseudojets.size(); i_tj++) {

      //  const TruthParticle* part = static_cast<const TruthParticle*>(&tagged_jets[i_tj]);
        const PseudoJet* tag_pjet = static_cast<const PseudoJet*>(&tagged_pseudojets[i_tj]);
      // in previous code theres a pt selection here, which I do in fastjet via a selector

        double mindr = DBL_MAX;
        double maxpt = 0;
        int mindrjetidx = -1;
        int maxptjetidx = -1;
        for (unsigned int iJet = 0; iJet < jets.size(); iJet++) {

            const Jet& jet = *jets.at(iJet);

            double pt = jet.pt();
 //           if (pt < m_jetptmin)
   //             continue;

            //double dr = jet.p4().DeltaR(JetFourMom_t(tag_pjet->pt(), tag_pjet->eta(), tag_pjet->phi(), tag_pjet->m()));
            const double pi = 3.14159265358979323846; //im a silly guy
            double drap = abs(jet.rapidity() - tag_pjet->rap());
            double dphi = abs(jet.phi() - tag_pjet->phi());
            if (dphi > pi) dphi = 2*pi - dphi;
            double dr = pow(drap*drap + dphi*dphi, 0.5);
            // too far for matching criterion
            if (dr > m_drmax)
                continue;

            // store the matched jet
            if (dr < mindr) {
                mindr = dr;
                mindrjetidx = iJet;
            }

            if (pt > maxpt) {
                maxpt = pt;
                maxptjetidx = iJet;
            }
        }

        // store the label particle with the jet
        if (mindrmatch && mindrjetidx >= 0)
            jetlabelparts.at(mindrjetidx).push_back(tag_pjet);
        else if (maxptjetidx >= 0)
            jetlabelparts.at(maxptjetidx).push_back(tag_pjet);
            
    }
   tellme.close();
    return jetlabelparts;
}

//vector<vector<const TruthParticle*> >
//ParticleJetDeltaRLabelTool::match(
//        const TruthParticleContainer& parts,
//        const JetContainer& jets) const {
//
//  ATH_MSG_VERBOSE("In " << name() << "::match()");
//  double partptmin = 5000; // default values hardcoded, to be able to set stuff just to IFN
//  double jetptmin = 10000;
//    vector<vector<const TruthParticle*> > jetlabelparts(jets.size(), vector<const TruthParticle*>());
//
//    // determine the match mode
//    bool mindrmatch;
//    if (m_matchmode == "MinDR")
//        mindrmatch = true;
//    else if (m_matchmode == "MaxPt")
//        mindrmatch = false;
//    else {
//        ATH_MSG_FATAL("MatchMode must be MinDR or MaxPt");
//        return jetlabelparts;
//    }
//
//    // loop over particles and find the best matched jet
//    for (TruthParticleContainer::const_iterator part_itr = parts.begin();
//            part_itr != parts.end(); ++part_itr) {
//
//        const TruthParticle* part = *part_itr;
//
//        if (part->pt() < partptmin)
//            continue;
//
//        double mindr = DBL_MAX;
//        double maxpt = 0;
//        int mindrjetidx = -1;
//        int maxptjetidx = -1;
//        for (unsigned int iJet = 0; iJet < jets.size(); iJet++) {
//
//            const Jet& jet = *jets.at(iJet);
//
//            double pt = jet.pt();
//            if (pt < jetptmin)
//                continue;
//
//            double dr = jet.p4().DeltaR(part->p4());
//            // too far for matching criterion
//            if (dr > m_drmax)
//                continue;
//
//            // store the matched jet
//            if (dr < mindr) {
//                mindr = dr;
//                mindrjetidx = iJet;
//            }
//
//            if (pt > maxpt) {
//                maxpt = pt;
//                maxptjetidx = iJet;
//            }
//        }
//
//        // store the label particle with the jet
//        if (mindrmatch && mindrjetidx >= 0)
//            jetlabelparts.at(mindrjetidx).push_back(part);
//        else if (maxptjetidx >= 0)
//            jetlabelparts.at(maxptjetidx).push_back(part);
//    }
//
//    return jetlabelparts;
//}

