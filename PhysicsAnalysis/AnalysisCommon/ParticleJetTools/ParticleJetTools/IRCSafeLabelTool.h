/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

#ifndef IRCSAFELABEL_H
#define IRCSAFELABEL_H

#include "AsgDataHandles/ReadHandleKey.h"
#include "AsgTools/AsgTool.h"
#include "JetInterface/IJetDecorator.h"
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "FlavInfo.hh"
#include "ParticleJetTools/ParticleJetLabelCommon.h"
#include "radekjet/PseudoJet.hh"


class IRCSafeLabelTool : public asg::AsgTool, public IJetDecorator {
ASG_TOOL_CLASS(IRCSafeLabelTool, IJetDecorator)
public:

  /// Constructor
  IRCSafeLabelTool(const std::string& name);

  StatusCode initialize() override;

  StatusCode decorate(const xAOD::JetContainer& jets) const override;



protected:
// function matching truth pseudojets to reco jets
std::vector< std::vector<const radekjet::PseudoJet*> > match(
                                                  std::vector<radekjet::PseudoJet>& tagged_jets,
                                                  const xAOD::JetContainer& jets) const;
// old match function                                                  
//std::vector<std::vector<const xAOD::TruthParticle*> > match(
//                                                  const xAOD::TruthParticleContainer& parts,
//                                                  const xAOD::JetContainer& jets) const;
  /// Name of jet label attributes
  ParticleJetTools::IRCSafeLabelNames m_ircsafelabelnames;
  std::unique_ptr<ParticleJetTools::IRCSafeLabelDecorators> m_ircsafelabeldecs;
//  std::string m_taulabelname;
//  std::string m_bottomlabelname;
//  std::string m_charmlabelname;

  /// Read handles particle collections for labeling. We need all inputs used for jet clustering
  SG::ReadHandleKey<xAOD::TruthParticleContainer> m_tauPartCollectionKey{this,"TauParticleCollection","","ReadHandleKey for tauPartCollection"};
  SG::ReadHandleKey<xAOD::TruthParticleContainer> m_bottomPartCollectionKey{this,"BParticleCollection","","ReadHandleKey for bottomPartCollection"};
  SG::ReadHandleKey<xAOD::TruthParticleContainer> m_charmPartCollectionKey{this,"CParticleCollection","","ReadHandleKey for charmPartCollection"};
  SG::ReadHandleKey<xAOD::TruthParticleContainer> m_outTruthPartKey{this, "TruthParticleCollection","", "ReadHandleKey of the TruthParticle collection"};

  // linkers to the truth particles
  // std::unique_ptr<ParticleJetTools::IParticleLinker> m_blinker;
  // std::unique_ptr<ParticleJetTools::IParticleLinker> m_clinker;
  // std::unique_ptr<ParticleJetTools::IParticleLinker> m_taulinker;

  /// Minimum pT for truth jet to be matched (in MeV)
  double m_truthjetptmin;

  /// Minimum pT for jet selection (in MeV)
  double m_jetptmin;

  /// Maximum dR for matching criterion
  double m_drmax;

  /// Matching mode: can be MinDR or MaxPt
  std::string m_matchmode;
};


#endif
